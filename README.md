# Grunnleggende Git-kommandoer

For å åpne VSCode sin kommando linje, trykk `Ctrl + Shift + P`. Hvis du da skriver `git` vil mange git kommandoer komme opp. Dette er kommandoene man bruker mest.

## clone
`git clone <url>` brukes til å kopiere et eksisterende Git-repositorium fra en annen plassering, som en fjernserver.

## stage/add
`git add <filnavn>` legger til en fil til "staging area" i forberedelse for en commit.

## commit
`git commit -m "<melding>"` lagrer endringene du har staged med en tilhørende melding for å beskrive endringene.

## push
`git push <remote> <branch>` sender dine committede endringer til en fjernserver.

## push upstream
`git push --set-upstream <remote> <branch>` setter opp en kobling mellom din lokale branch og en fjernbranch, og pusher endringene dine.

## status
`git status` viser statusen til arbeidskatalogen og "staging area".

## fetch og pull
`git fetch <remote>` henter data fra en fjernserver uten å merge det inn i din lokale kode.
`git pull <remote> <branch>` henter data fra en fjernserver og prøver å merge det inn i din lokale kode.

## merge
`git merge <branch>` slår sammen endringene fra den angitte branchen inn i den aktive branchen.

## merge conflict
En merge conflict oppstår når Git ikke klarer å løse forskjeller i koden mellom to branches. Du må manuelt løse disse konfliktene.

## .gitignore
`.gitignore` er en fil som forteller Git hvilke filer eller mapper den skal ignorere.

## .gitkeep
`.gitkeep` er ikke en offisiell Git-fil. Det er en tom fil som brukes til å inkludere tomme mapper i Git. Git ignorerer vanligvis tomme mapper.

# God praksis i Git

1. **Kommitt ofte:** Dette gjør det lettere å forstå hva hver commit gjør, og det blir enklere å rulle tilbake endringer om nødvendig.

2. **Skriv meningsfulle commit-meldinger:** En god commit-melding beskriver hva endringen gjør, ikke hvordan den gjør det.

3. **Hold endringer atskilt:** Hver commit bør være en atskilt endring. Hvis du fikser to bugs, bør det være to forskjellige commits.

4. **Unngå å committe uferdig arbeid:** Hvis du trenger å lagre uferdig arbeid, kan du bruke `git stash` i stedet for å committe det.

5. **Test før du committer:** Sørg for at koden din fungerer som den skal før du committer den.

6. **Ikke endre publiserte historie:** Hvis du har pushet commits til en delt branch eller til master, bør du unngå å endre disse commitsene.

7. **Bruk branches:** Bruk branches for nye funksjoner eller bugfixes. Dette holder master-branchen ren og klar for produksjon.

8. **Merge med omhu:** Når du merger, sørg for at du forstår endringene du merger inn. Hvis det er merge-konflikter, løs dem på en fornuftig måte.

9. **Bruk .gitignore:** Legg filer og mapper som ikke bør være i Git (som systemfiler, passordfiler, bygde filer) i en .gitignore-fil.

10. **Hold deg oppdatert:** Bruk `git pull` regelmessig for å holde din lokale kopi oppdatert med den siste koden.